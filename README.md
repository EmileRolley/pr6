# Tools for the course *Programmation Réseaux*

<!-- vim-markdown-toc GFM -->

* [`net` a convenient C lib for networking 🌐](#net-a-convenient-c-lib-for-networking-)
	* [How to..](#how-to)
		* [Compile](#compile)
			* [Documentation](#documentation)
		* [Use](#use)
	* [Examples](#examples)
		* [Retrieve an address from an host/service name](#retrieve-an-address-from-an-hostservice-name)
		* [TCP server quick start](#tcp-server-quick-start)
			* [Concurrency](#concurrency)
		* [TCP client quick start](#tcp-client-quick-start)
* [Contribution 📨](#contribution-)
	* [Conventions](#conventions)
* [Contact 🙋](#contact-)
* [TODOs ✅](#todos-)

<!-- vim-markdown-toc -->

Link to the course : https://www.irif.fr/~sangnier/enseignement/reseaux.html

## `net` a convenient C lib for networking 🌐

**DISCLAIMER** : It's handmade without serious tests, therefore, do not rely on it blindly.

### How to..

#### Compile

To create the static lib `./libnet.a` :

```shell
make
```

##### Documentation

The documentation could be compiled with [Doxygen](https://github.com/doxygen/doxygen) :

```shell
make doc
```

or

```shell
doxygen Doxyfile
```

When the documentation is compiled, open *./libnet/doc/html/index.html* with
your favorite web browser.

#### Use

In order to use the lib :

1. Put the `libnet/net.h` in your current folder.
2. Add `#include "net.h"` at the beginning of your file.
3. Run `gcc prog.c -L <path/to/libnet.a> -lnet -o prog`

> **For the moment, a simpler way is to copy `net.h` and `net.c` in your project
> before the compilation.**

### Examples

#### Retrieve an address from an host/service name

Getting the `IPv4` address :

```c
struct in_addr addr;

if (OK == net_applyto_sockaddr("hostname", "service", AF_INET, FALSE, net_getaddr, &addr)) {
	printf("Address IPv4 : %s\n", inet_ntoa(addr));
}
```

Getting the `IPv6` address :

```c
struct in6_addr addr6;

if (OK == net_applyto_sockaddr("hostname", "service", AF_INET6, FALSE, net_getaddr6, &addr6)) {
	// Manage `addr6`
}
```

> For more information about `net_applyto_sockaddr` look to the source code.

#### TCP server quick start

Simple program for hosting a service on a TCP server binded and listening on
the port `4242`.

```c
#include "net.h"

void simple_service(struct sockaddr sacaller, int const scaller, void *arg);

int main(void) {
	net_qs_tcpserver(4242, simple_service, "Connexion achieved !\n");
	return 0;
}
```

Service example :

```c
void simple_service(struct sockaddr sacaller, const int scaller, void *arg) {
	char buff[100];
	int rec;
	const char *msg = (char *) arg;
	struct sockaddr_in saddrin = *(struct sockaddr_in *) &sacaller;

	printf("Caller socket port : %d\n", ntohs(saddrin.sin_port));
	printf("Caller socket addr : %s\n", inet_ntoa(saddrin.sin_addr));

	send(scaller, msg, strlen(msg) * sizeof(char), 0);
	rec = recv(scaller, buff, 99 * sizeof(char), 0);
	buff[rec]='\0';
	printf("Received message : %s\n", buff);
}
```

##### Concurrency

For a threaded version :

```c
#include "net.h"

void *start_routine(void *arg);

int main(void) {
	net_qs_conctcpserver(4242, start_routine);
	return 0;
}
```

**Note**: for now the caller's socket is given to the `start_routine`.

#### TCP client quick start

In order to create a TCP client connected to the localhost `127.0.0.1` on port `4242`.

```c
#include "net.h"

void simple_service(struct sockaddr saddr, const int sock, void *arg);

int main(void) {
	net_qs_tcpclient(4242, "127.0.0.1", simple_service, "Connexion achieved !\n");
	return 0;
}
```


## Contribution 📨

Feel free to contribute and give feedback !

### Conventions

* For style formating, please refert to [C Coding
Standard](https://users.ece.cmu.edu/~eno/coding/CCodingStandard.html).
* For commits, please refer to [Conventionnal Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## Contact 🙋

| Author       | Discord            | Mail                   |
|--------------|--------------------|------------------------|
| Emile ROLLEY | `EmileRolley#4142` | `emile.rolley@tuta.io` |

## TODOs ✅

* [ ] Add concurrency support.
  * [x] Quick start threaded TCP server.
  * [ ] Quick start threaded TCP client.
* [ ] Add automated tests.
* [ ] Add more flexibility/features.
