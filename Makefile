.PHONY: all

CC = gcc
CFLAGS = -Wall -pthread

all: libnet

libnet: libnet/net.o
	ar rcs $@.a $?

doc:
	doxygen Doxyfile

clean:
	rm -r libnet/doc
	rm libnet/*.o libnet.a
