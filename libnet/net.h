/**
 *     File Name           :     libnet/net.h
 *     Created By          :     Emile Rolley
 *     Creation Date       :     [2021-02-11 11:20]
 *     Last Modified       :     [2021-02-16 16:04]
 **/

#ifndef NET_H

#include <assert.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>

#define OK 0
#define ERR -1

#define TRUE 1
#define FALSE 0

#define NET_SADDRIN_SIZE sizeof(struct sockaddr_in)

/**
 * Quick start for a concurrent TCP server hosting a service in a thread.
 *
 * Basically, binds a server socket to the given port and in an infinite loop
 * calls start_routine in a pthread.
 *
 * @note The argument given to the start_routine is the caller socket.
 *
 * NOTE: Needs to find a way to manage the start_routine arguement (polymorphism).
 *
 * @param port Is the port to bind with.
 * @param start_routine Is the routine run in the thread.
 *
 * @return ERR if the server could not be launched, otherwise, OK.
 */
int net_qs_conctcpserver(
	const int port,
	void *(*start_routine) (void *arg)
);

/**
 * Quick start for a TCP server hosting a service.
 *
 * Basically, binds a server socket to the given port and in an infinite loop
 * calls `serve` with all accepted caller socket.
 *
 * @note The server use `struct sockaddr_in` to accept the caller, therefore,
 * in the service implementation (`serve`) `sacaller` could be cast into a
 * `struct sockaddr_in`.
 *
 * @param port Is the port to bind with.
 * @param serve Is the service to host on the TCP server.
 * @param arg Is the argument given to `service`.
 *
 * @return ERR if the server could not be launched, otherwise, OK.
 */
int net_qs_tcpserver(
	const int port,
	void (*serve) (struct sockaddr sacaller, const int scaller, void *arg),
	void *arg
);

/**
 * Quick start for a TCP server hosting a service from a `sockaddr`.
 *
 * Basically, binds a server socket to the given port and in an infinite loop
 * calls `serve` with all accepted caller socket.
 *
 * @param address_sock Is the socket address to bind with.
 * @param serve Is the service to host on the TCP server.
 * @param arg Is the argument given to `service`.
 *
 * @return ERR if the server could not be launched, otherwise, OK.
 */
int net_qs_tcpserver_fromaddrsock(
	const struct sockaddr *address_sock,
	void (*serve) (struct sockaddr sacaller, const int scaller, void *arg),
	void *arg
);

/**
 * Quick start for TCP client running a routine using IPv4 address.
 *
 * @note IPv4 address = `struct sockaddr_in`, therefore, in the routine
 * implementation (`run`) `sockaddr` could be cast into a `struct sockaddr_in`.
 *
 * @param port Is the port to connect with.
 * @param address Is the IPv4 address to connect with.
 * @param run Is the routine to run.
 * @param arg Is the argument given to `run`.
 *
 * @return ERR if the client could not connect proprely, otherwise, OK.
 */
int net_qs_tcpclient(
	const int port,
	const char *address,
	void (*run) (struct sockaddr sockaddr, const int sock, void *arg),
	void *arg
);

/**
 * Quick start for TCP client running a routine using a `struct sockaddr`.
 *
 * @param address_sock Is the socket address to connect with.
 * @param run Is the routine to run.
 * @param arg Is the argument given to `run`.
 *
 * @return ERR if the client could not connect proprely, otherwise, OK.
 */
int net_qs_tcpclient_fromaddrsock(
	const struct sockaddr *address_sock,
	void (*run) (struct sockaddr sockaddr, const int sock, void *arg),
	void *arg
);

/**
 * Iterates through the `addrinfo` returned by `getaddrinfo` and applies consume to them.
 *
 * @param node Is the wanted internet host.
 * @param service Is the wanted internet service.
 * @param family Is the address family (IPv4 or IPv6).
 * @param to_all Is a boolean wich specified if `consume` needs to be applied to all `addrinfo`.
 * @param consume Is the pointer to the function to apply.
 * @param args Is the argument wich will be given to `consume` with the `addrinfo->ai_addr`.
 *
 * @return OK if an address was found, otherwise ERR.
 */
int net_applyto_sockaddr(
	const char *node,
	const char *service,
	const int family,
	const int to_all,
	void (*consume) (struct sockaddr *, void *),
	void *args
);

/**
 * Copies the `saddr->sin_addr` to `addr`.
 *
 * @note Is made for being used with `net_applyto_sockaddr`.
 */
void net_getaddr(struct sockaddr *saddr, void *addr);

/**
 * Copies the `saddr->sin6_addr` to `addr`.
 *
 * @note Is made for being used with `net_applyto_sockaddr`.
 */
void net_getaddr6(struct sockaddr *saddr, void *addr);

#endif
