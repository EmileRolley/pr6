/**
 *     File Name           :     libnet/net.c
 *     Created By          :     Emile Rolley
 *     Creation Date       :     [2021-02-11 11:21]
 *     Last Modified       :     [2021-02-16 16:04]
 **/

#include "net.h"

static void net_init_sockaddrin(const int family, const int port, struct sockaddr_in *saddrin) {
	saddrin->sin_family = family;
	saddrin->sin_port = htons(port);
}

int net_qs_conctcpserver(
	const int port,
	void *(*serve) (void *arg)
) {
	int sock;
	struct sockaddr_in address_sock;

	net_init_sockaddrin(AF_INET, port, &address_sock);
	address_sock.sin_addr.s_addr = htonl(INADDR_ANY);

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (ERR == bind(sock, (struct sockaddr *) &address_sock, NET_SADDRIN_SIZE)) {
		perror("net: bind error");
		close(sock);
		return ERR;
	}

	if (ERR == listen(sock, 0)) {
		perror("net: listen error");
		close(sock);
		return ERR;
	}

	while (1) {
		struct sockaddr_in sacaller;
		socklen_t sacaller_len;
		int *scaller;

		sacaller_len = sizeof(sacaller);
		scaller = malloc(sizeof(int));
		assert(NULL != scaller);

		*scaller = accept(sock, (struct sockaddr *) &sacaller, &sacaller_len);
		if (0 <= scaller) {
			pthread_t thread;

			if (OK != pthread_create(&thread, NULL, serve, scaller)) {
				perror("net: pthread_create error");
				close(*scaller);
				close(sock);
				return ERR;
			}
		}
	}
	return OK;
}

int net_qs_tcpserver(
	const int port,
	void (*serve) (struct sockaddr sacaller, const int scaller, void *arg),
	void *arg)
{
	struct sockaddr_in address_sock;

	net_init_sockaddrin(AF_INET, port, &address_sock);
	address_sock.sin_addr.s_addr = htonl(INADDR_ANY);

	return net_qs_tcpserver_fromaddrsock((struct sockaddr *) &address_sock, serve, arg);
}

int net_qs_tcpserver_fromaddrsock(
	const struct sockaddr *address_sock,
	void (*serve) (struct sockaddr sacaller, const int scaller, void *arg),
	void *arg)
{
	int sock;

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (ERR == bind(sock, address_sock, NET_SADDRIN_SIZE)) {
		perror("net: bind error");
		close(sock);
		return ERR;
	}

	if (ERR == listen(sock, 0)) {
		perror("net: listen error");
		close(sock);
		return ERR;
	}

	while (1) {
		struct sockaddr_in sacaller;
		socklen_t sacaller_len = sizeof(sacaller);
		int scaller;

		if (0 <= (scaller = accept(sock, (struct sockaddr *) &sacaller, &sacaller_len))) {
			serve(*(struct sockaddr *) &sacaller, scaller, arg);
			close(scaller);
		}
	}

	return OK;
}

int net_qs_tcpclient(
	const int port,
	const char *address,
	void (*run) (struct sockaddr sockaddr, const int sock, void *arg),
	void *arg)
{
	struct sockaddr_in address_sock;

	net_init_sockaddrin(AF_INET, port, &address_sock);
	inet_aton(address, &address_sock.sin_addr);

	return net_qs_tcpclient_fromaddrsock((struct sockaddr *) &address_sock, run, arg);
}

int net_qs_tcpclient_fromaddrsock(
	const struct sockaddr *address_sock,
	void (*run) (struct sockaddr sockaddr, const int sock, void *arg),
	void *arg)
{
	int sock;

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (ERR == connect(sock, address_sock, NET_SADDRIN_SIZE)) {
		perror("net: connect error");
		close(sock);
		return ERR;
	}

	run(*address_sock, sock, arg);

	close(sock);
	return OK;
}

int net_applyto_sockaddr(
	const char *node,
	const char *service,
	const int family,
	const int to_all,
	void (*consume) (struct sockaddr *, void *),
	void *args)
{
	int ret_val = ERR;
	struct addrinfo *hd_info;
	struct addrinfo *info;
	struct addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = family;

	if (OK == (ret_val = getaddrinfo(node, service, &hints, &hd_info))) {
		info = hd_info;
		while (NULL != info) {
			consume(info->ai_addr, args);
			ret_val = OK;
			info = info->ai_next;
			if (!to_all) {
				break;
			}
		}
	}

	freeaddrinfo(hd_info);
	return ret_val;
}

void net_getaddr(struct sockaddr *saddr, void *addr) {
	memcpy(
		(struct in_addr *) addr,
		&(((struct sockaddr_in *) saddr)->sin_addr),
		sizeof(struct in_addr)
	);
}

void net_getaddr6(struct sockaddr *saddr, void *addr) {
	memcpy(
		(struct in6_addr *)addr,
		&(((struct sockaddr_in6 *) saddr)->sin6_addr),
		sizeof(struct in6_addr)
	);
}
